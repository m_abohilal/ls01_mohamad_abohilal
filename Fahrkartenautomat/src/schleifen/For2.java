package schleifen;

public class For2 {
	
	public static void main(String[] args) {



		System.out.print("a) ");
		for(int i= 99; i>=9;i-=3) {
			
			System.out.print(i+", ");
		}
		System.out.print("\n");


		System.out.print("b) ");
		for(int i= 1; i<400;i++) {
			
			System.out.print(i+", ");
		}
		System.out.print("\n");



		System.out.print("c) ");
		for(int i= 2; i<=102;i+=4) {
			
			System.out.print(i+", ");
		}
		System.out.print("\n");
	

	
		System.out.print("d) ");
		for(int i= 2; i<=32; i=i+2 ) {
			
			System.out.print(i*i+", ");
		}
		System.out.print("\n");


		System.out.print("e) ");
		for(int i= 2; i<=32768;i*=2) {
			
			System.out.print(i+", ");
		}
		System.out.print("\n");
	}


}

