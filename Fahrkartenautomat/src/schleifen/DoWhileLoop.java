package schleifen;

import java.util.Scanner;


public class DoWhileLoop {
	private static Scanner tastatur;
	
	public static void main(String[] args) {
		
		tastatur = new Scanner(System.in);
		
		System.out.println("Bitte geben Sie eine Nummer ein ");
		int n = tastatur.nextInt();
		
		int i = 1;
		do {
			
			System.out.println(i);
			  i++;
			
		} 
		while (i <= n);
		
		System.out.println("Bitte geben Sie eine Nummer ein ");
		int j = tastatur.nextInt();
		
		
		int c = j;
		do {
			  System.out.println(c);
			  c--;
			
		}
		while (c >= 1);

}
}

