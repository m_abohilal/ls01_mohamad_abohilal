﻿import java.util.Scanner;

class Fahrkartenautomat {
	private static Scanner tastatur;

	public static void main(String[] args) {

		double zuZahlenderBetrag = fahrkartenbestellungErfasse();

		fahrkartenBezahlen(zuZahlenderBetrag);

		warte(750);

		fahrkartenAusgeben();

	}

	private static double fahrkartenbestellungErfasse() {
		tastatur = new Scanner(System.in);

		System.out.print("Ticketpreis (Euro): ");
		double ticketsPreis = tastatur.nextDouble();
		
		if (ticketsPreis < 0) {
			System.out.println("Fehler, Preis wird auf 1,00€ gesetzt. ");
			ticketsPreis = 1; 
		}
		
		System.out.print("Anzahl der Tickets (Bitte nur zwichen 1 und 10): ");
		double anzahlDerTickets = tastatur.nextDouble();
		double zuZahlenderBetrag;
		
		if (anzahlDerTickets >= 1 && anzahlDerTickets <= 10) {

			zuZahlenderBetrag = ticketsPreis * anzahlDerTickets;
			
			}
		
		else {
			
			System.out.println("Falsche eingabe, Der System führt mit 1 weiter.");
			zuZahlenderBetrag = ticketsPreis * 1;
			
		}
		
		System.out.printf("Zu zahlender Betrag: %.2f Euro\n", zuZahlenderBetrag);

		return zuZahlenderBetrag;

	}

	private static double fahrkartenBezahlen(double zuZahlenderBetrag) {

		double eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.println("Noch zu zahlen: " + (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			double eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}

		System.out.println("\nFahrschein wird ausgegeben");

		System.out.println("\n\n");

		return rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
	}

	private static double rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {

		double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		if (rückgabebetrag > 0.0) {
			System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				//System.out.println("2 EURO");
				muenzeAusgeben(2 , " Euro");
				rückgabebetrag -= 2.0;
			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				//System.out.println("1 EURO");
				muenzeAusgeben(1 , " Euro");
				rückgabebetrag -= 1.0;
			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				//System.out.println("50 CENT");
				muenzeAusgeben(50 , " Cent");
				rückgabebetrag -= 0.5;
			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				//System.out.println("20 CENT");
				muenzeAusgeben(20 , " Cent");
				rückgabebetrag -= 0.2;
			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				//System.out.println("10 CENT");
				muenzeAusgeben(10, " Cent");
				rückgabebetrag -= 0.1;
			}
			while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				//System.out.println("5 CENT");
				muenzeAusgeben(5 , " Cent");
				rückgabebetrag -= 0.05;
			}
		}
		return rückgabebetrag;
	}

	private static void fahrkartenAusgeben() {

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.");
	}

	static void warte(int millisekunde) {
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(millisekunde);
			} catch (InterruptedException e) {

				e.printStackTrace();
			}

		}
	}
	
	static void muenzeAusgeben(int betrag, String einheit) {
		System.out.println(betrag + einheit);
	
	}
	
}

// 5. Wir können für diese Aufgabe 2 Datentypen verwenden, entweder Float oder Double, ich habe mich für double entschieden, da er genauer ist. 

// 6. Zuerst fragt das Programm den Benutzer nach seinem Ticketpreis, dann fragt es ihn noch mal nach dem Anzahl der Tickets. 
//    Dann durch einen Befehl multipliziert der Code den Ticketpreis mit dem Anzahl der Tickets und ziegt ihn den Benutzer an. 
// Git

// Test Test
