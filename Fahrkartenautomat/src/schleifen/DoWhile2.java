package schleifen;
import java.util.Scanner;
public class DoWhile2 {
	
	public static void main(String[] args) { 
		
		System.out.println("Geben Sie in der Konsole eine Zahl ein:");
		
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();

		ErsterDoWhile(n);
		ZweiterDoWhile(n); 
		scanner.close();
	}

	/**
	 * @category ErstDoWhile
	 */
	public static void ErsterDoWhile(int n) {

		int i = 1;
		
		do {
			System.out.print(i+ ", ");
			i++;
		} while (i <= n);
		System.out.print("\n");
		
		
	} 
	
	/**
	 * @category ZweiterDoWhile
	 */
	public static void ZweiterDoWhile(int n) {

		int i = n;
		
		do {
			System.out.print(i+ ", ");
			i--;
		} while (i >= 1);
		
		
	}

}
