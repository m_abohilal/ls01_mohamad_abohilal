package schleifen;

import java.util.Scanner;

public class WhileLoop {
	private static Scanner tastatur;
	
	public static void main(String[] args) {
		
		tastatur = new Scanner(System.in);
		
		System.out.println("Bitte geben Sie eine Nummer ein ");
		int n = tastatur.nextInt();
		
		int i = 1;
		while (i <= n) {
		  System.out.println(i);
		  i++;
		}
		
		System.out.println("Bitte geben Sie eine Nummer ein ");
		int j = tastatur.nextInt();
		
		int c = j;
		while (c >= 1) {
		  System.out.println(c);
		  c--;
		}
}
}
	


