package schleifen;

import java.util.Scanner;

public class Forloop {
private static Scanner tastatur;
	
	
	public static void main(String[] args) {
	
	tastatur = new Scanner(System.in);
	
	System.out.println("Bitte geben Sie eine Nummer ein ");
	int n = tastatur.nextInt();
	
	for (int i = 1; i <= n; i++) {
		
		  System.out.println(i);
		}
	
	System.out.println("Bitte geben Sie eine Nummer ein ");
	int j = tastatur.nextInt();
	
	for (int i = j; i >= 1; i--) {
		
		  System.out.println(i);
		}
	
}
}
